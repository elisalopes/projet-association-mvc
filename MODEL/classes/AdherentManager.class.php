<?php

function crypted_password ($password) {
    $options = [
        'cost' => 12,
    ];
    return password_hash($password, PASSWORD_BCRYPT, $options);
}

class AdherentManager {

    private $_db;

    public function __construct($db) {
        $this->setDb($db);
    }

    public function setDb ($db) {
        $this->_db = $db;
    }

    public function createAdherent($adherent) {
        try{ 
            $query = $this->_db->prepare("
                INSERT INTO adherent (adherent_last_name, adherent_first_name, adherent_pseudo, adherent_password, adherent_email, adherent_phone_number, adherent_address, adherent_number, adherent_subscription, adherent_register_date, adherent_is_admin) VALUES (:last_name, :first_name, :pseudo, :password, :email, :phone_number, :address, :number, :subscription, CURRENT_DATE, 0)
            ");
            $query->bindValue(':last_name', $adherent->getAdherentLastName());
            $query->bindValue(':first_name', $adherent->getAdherentFirstName());
            $query->bindValue(':pseudo', $adherent->getAdherentPseudo());
            $query->bindValue(':password', crypted_password($adherent->getAdherentPassword()));
            $query->bindValue(':email', $adherent->getAdherentEmail());
            $query->bindValue(':phone_number', $adherent->getAdherentPhoneNumber());
            $query->bindValue(':address', $adherent->getAdherentAddress());
            $query->bindValue(':number', $adherent->getAdherentNumber());
            $query->bindValue(':subscription', $adherent->getAdherentSubscription());

            $query->execute() or die(print_r($query->errorInfo(), true));
            $adherent_id = $this->_db->lastInsertId();
            $query = $this->_db->query("SELECT * FROM adherent WHERE adherent_id = $adherent_id");
            return $query->fetch(PDO::FETCH_ASSOC);

        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function getAdherent($pseudo, $password) { // verify password and get adherent if exists
        try{
            $query = $this->_db->prepare("
                SELECT * FROM adherent WHERE adherent_pseudo = ?
            ");
            $query->execute(array($pseudo));
            $result = $query->fetch(PDO::FETCH_ASSOC);
            if (password_verify($password, $result['adherent_password'])) {
                return $result;
            } else {
                return NULL;
            }
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function getAdherentAdmin($pseudo, $password, $is_admin) { // verify password/pseudo/is_admin and get admin if exists
        try{
            $query = $this->_db->prepare("
                SELECT * FROM adherent WHERE adherent_pseudo = ? AND adherent_is_admin = 1
            ");
            $query->execute(array($pseudo));
            $result = $query->fetch(PDO::FETCH_ASSOC);
            if ( (password_verify($password, $result['adherent_password']))   ) {
                return $result;
            } else {
                return NULL;
            }
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function existsAdherentNumber($number) {
        try{
            $query = $this->_db->prepare("
                SELECT COUNT(adherent_id) FROM adherent WHERE adherent_number = ?
            ");
            $query->execute(array($number));
            $result = $query->fetch();
            return $result[0] > 0;
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function existsAdherentEmail($email) {
        if (empty($email)){
            return false;
        }
        try{
            $query = $this->_db->prepare("
                SELECT COUNT(adherent_id) FROM adherent WHERE adherent_email = ?
            ");
            $query->execute(array($email));
            $result = $query->fetch();
            return $result[0] > 0;
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function existsAdherentPseudo($pseudo) {
        if (empty($pseudo)) {
            return false;
        }
        try{
            $query = $this->_db->prepare("
                SELECT COUNT(adherent_id) FROM adherent WHERE adherent_pseudo = ?
            ");
            $query->execute(array($pseudo));
            $result = $query->fetch();
            return $result[0] > 0;
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function updateAdherent($adherent) {
        try{
            //requetes 
            $query = $this->_db->prepare("
                UPDATE adherent SET 
                adherent_last_name = :last_name,
                adherent_first_name = :first_name,
                adherent_pseudo = :pseudo,
                adherent_email = :email,
                adherent_phone_number = :phone_number,
                adherent_address = :address
                WHERE adherent_id = :id
            ");
            $query->bindValue(':id', $adherent->getAdherentId());
            $query->bindValue(':last_name', $adherent->getAdherentLastName());
            $query->bindValue(':first_name', $adherent->getAdherentFirstName());
            $query->bindValue(':pseudo', $adherent->getAdherentPseudo());
            $query->bindValue(':email', $adherent->getAdherentEmail());
            $query->bindValue(':phone_number', $adherent->getAdherentPhoneNumber());
            $query->bindValue(':address', $adherent->getAdherentAddress());

            $query->execute() or die(print_r($query->errorInfo(), true));
            $adherent_id = $adherent->getAdherentId();
            $query = $this->_db->query("SELECT * FROM adherent WHERE adherent_id = $adherent_id");
            return $query->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function updateAdherentPassword($id, $password) {
        try{
            $query = $this->_db->prepare("
                UPDATE adherent SET adherent_password = :password WHERE adherent_id = :id
            ");
            $query->bindValue(':id', $id);
            $query->bindValue(':password', crypted_password($password));
            $query->execute();
            return true;
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }
}