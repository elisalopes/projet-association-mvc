<?php



class DonationPage extends Controller{
        
    private $_donation_id;
    private $_donation_value;
    private $_donation_date;
    private $_donation_last_name;
    private $_donation_first_name;
    private $_donation_email;
    private $_donation_phone_number;
    private $_donation_address;
    // private $_donation_is_visible;
    private $_adherent_pseudo;
    private $_adherent_password;

    private $_errors = array(
        "missing_donation_value" => false,
        "missing_donation_date" => false,
        "missing_donation_last_name" => false,
        "missing_donation_first_name" => false,
        "missing_donation_email" => false,
        "incorrect_donation_email" => false,
        "missing_donation_address" => false,
        "missing_donation_phone" => false,
        "incorrect_donation_phone" => false,
        // "missing_donation_is_visible" => false,
        ///////////////////////////////
        "missing_adherent_pseudo"=>false,
        "incorrect_adherent_pseudo"=> false,
        "missing_adherent_password"=> false,
        "incorrect_adherent_password"=> false
    );

    public function __construct($valeurs = []) {
        $this->_donation_manager = new DonationManager(connection());
    
        if (!empty($valeurs)) 
        {
            $this->hydrate($valeurs);
        }
    }

    public function hydrate($donnees) {
        foreach ($donnees as $attribut => $valeur)
        {
            $methode = 'set'.str_replace('_', '', ucwords($attribut, '_'));
            
            if (is_callable([$this, $methode]))
            {
                $this->$methode($valeur);
            }
        }
    }

    public function isValid() { // verify if everything is okay to be send to the db
        $inputs = ['donation_value', 'donation_date','donation_last_name', 'donation_first_name', 'donation_email', 'donation_phone_number', 'donation_address', 'donation_is_visible'];

        foreach($inputs as $key => $value) {
            $temp = '_donation_'.$value;
            if(empty($this->$temp)) {
                $this->_errors["missing_donation".$value] = true;
            }   
        }

        foreach($this->_errors as $key => $value) {
            if ($value == true) {
                return false;
            }
        }
        return true;
    }



    public function setDonationId ($donation_id) {
        $this->_donation_id= $donation_id; 
    }
    
    public function getDonationId () {
        return $this->_donation_id;
    }

    public function setDonationDate ($donation_date) {
        $this->_donation_register_date= $donation_date;
    }
    
    public function getDonationDate () {
        return $this->_donation_register_date;
    }
    public function setDonationLastName ($donation_last_name) {
        $this->_donation_last_name= $donation_last_name; 
    }
    
    public function getDonationLastName () {
        return $this->_donation_last_name;
    }

    public function setDonationFirstName ($donation_first_name) {
        $this->_donation_first_name= $donation_first_name;
    }
    
    public function getDonationFirstName () {
        return $this->_donation_first_name;
    }

    public function setDonationEmail ($donation_email) {
        if (!verify_email($donation_email)) {
            $this->_errors["incorrect_email"] = true;
        } else {
            $this->_donation_email = $donation_email;
        }
        
    }
    public function getDonationEmail () {
        return $this->_donation_email;
    }
    
    public function setDonationPhoneNumber ($donation_phone) {
        if (!verify_phone_number($donation_phone)) {
            $this->_errors["incorrect_phone"] = true;
        } else {
            $this->_donation_phone_number= $donation_phone;
        }
        
    }
    
    public function getDonationPhoneNumber () {
        return $this->_donation_phone_number;
    }

    public function setDonationAddress ($donation_address) {
        $this->_donation_address= $donation_address;
    }
    
    public function getDonationAddress () {
        return $this->_donation_address;
    }

    public function setDonationValue ($donation_value) {
        $this->_donation_value= $donation_value;
    }
     
    public function getDonationValue () {
        return $this->_donation_value;
    }
    

    public function setDonationIsVisible($donation_is_visible) {
        $this->_donation_is_visible= $donation_is_visible; 
    }
    
    public function getDonationIsVisible() {
        return $this->_donation_is_visible;
    }


    public function setDonationAdherentPseudo($donation_adherent_pseudo) {
        $this->_donation_adherent_pseudo = $donation_adherent_pseudo; 
    }
    
    public function getDonationAdherentPseudo() {
        return $this->_donation_adherent_pseudo;
    }
    public function setDonationAdherentPassword($donation_adherent_password) {
        $this->_donation_adherent_password= $donation_adherent_password; 
    }
    
    public function getDonationAdherentPassword() {
        return $this->_donation_adherent_password;
    }

    //////////RAJOUTER GET SET POUR LES 3 TYPES D'ENREGISTREMENT DE DONS
    // createDonationAnonymous
    // createDonationAdherent
    // createDonationNotAdherent
    /////////////

    public function setError($error) {
        $this->_errors[$error] = true;
    }
    public function getErrors()
    {
      return $this->_errors;
    }
}
