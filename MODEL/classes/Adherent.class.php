<?php 

function verify_email ($input) {
    $pattern = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]{2,4}/";
    return preg_match($pattern, $input);
  
}

function verify_phone_number ($input) {
    $pattern = "/^(?:(?:\+|00)[0-9[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/";
    return preg_match($pattern, $input);
    ///^(?:(?:\+|00)[0-9[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/
  
}

function verify_password($input) {
    if (
        strlen($input) < 8 
        || !preg_match("#[0-9]+#",$input) 
        || !preg_match("#[A-Z]+#",$input) 
        || !preg_match("#[a-z]+#",$input)
    ) {
        return false;
    } else {
        return true;
    }
}

class Adherent{

    private $_adherent_id;
    private $_adherent_last_name;
    private $_adherent_first_name;
    private $_adherent_pseudo;
    private $_adherent_password;
    private $_adherent_email;
    private $_adherent_phone_number;
    private $_adherent_address;
    private $_adherent_number;
    private $_adherent_register_date;
    private $_adherent_is_admin = 0;
    private $_adherent_subscription;
    private $_adherent_manager;

    private $_errors = array(
        "missing_last_name" => false,
        "missing_first_name" => false,
        "missing_pseudo" => false,
        "already_exists_pseudo" => false,
        "missing_password" => false,
        "incorrect_password" => false,
        "missing_email" => false,
        "already_exists_email" => false,
        "incorrect_email" => false,
        "missing_address" => false,
        "missing_phone" => false,
        "incorrect_phone" => false,
        "missing_subscription" => false
    );
 
    public function __construct($valeurs = []) {
        $this->_adherent_manager = new AdherentManager(connection());
    
        if (!empty($valeurs)) // Si on a spécifié des valeurs, alors on hydrate l'objet.
        {
            $this->hydrate($valeurs);
        }
        if (empty($this->_adherent_number)) {
            $this->generateNumber();
        }
    }
    
    public function hydrate($donnees) {
        foreach ($donnees as $attribut => $valeur)
        {
            $methode = 'set'.str_replace('_', '', ucwords($attribut, '_'));
            
            if (is_callable([$this, $methode]))
            {
                $this->$methode($valeur);
            }
        }
    }

    private function generateNumber() { // TODO à ameliorer
        do {
            $number = rand(0, 9999);
        } while (
            $this->_adherent_manager->existsAdherentNumber($number)
        );
        $this->_adherent_number =  $number;
    }

    public function isNew() // return bool
    {
      return empty($this->_adherent_id);
    }
    
    public function isValid() { // verify if everything is okay to be send to the db
        $inputs = ['last_name', 'first_name', 'pseudo', 'password', 'email', 'phone_number', 'address', 'subscription'];

        foreach($inputs as $key => $value) {
            $temp = '_adherent_'.$value;
            if(empty($this->$temp)) {
                $this->_errors["missing_".$value] = true;
            }   
        }

        foreach($this->_errors as $key => $value) {
            if ($value == true) {
                return false;
            }
        }
        return true;
    }

    public function setAdherentId ($adherent_id) {
        $this->_adherent_id= $adherent_id; 
    }
    
    public function getAdherentId () {
        return $this->_adherent_id;
    }

    public function setAdherentLastName ($adherent_last_name) {
        $this->_adherent_last_name= $adherent_last_name; 
    }
    
    public function getAdherentLastName () {
        return $this->_adherent_last_name;
    }

    public function setAdherentFirstName ($adherent_first_name) {
        $this->_adherent_first_name= $adherent_first_name;
    }
    
    public function getAdherentFirstName () {
        return $this->_adherent_first_name;
    }

    public function setAdherentPseudo ($pseudo) {
        $this->_adherent_pseudo= $pseudo;
        
    }
    
    public function getAdherentPseudo () {
        return $this->_adherent_pseudo;
    }

    public function setAdherentPassword ($adherent_password) {
        if (!verify_password($adherent_password)) {
            $this->_errors["incorrect_password"] = true;
        } else {
            $this->_adherent_password= $adherent_password;
        }
    }
    
    public function getAdherentPassword () {
        return $this->_adherent_password;
    }


    public function setAdherentEmail ($adherent_email) {
        if (!verify_email($adherent_email)) {
            $this->_errors["incorrect_email"] = true;
        } else {
            $this->_adherent_email = $adherent_email;
        }
        
    }
    
    public function getAdherentEmail () {
        return $this->_adherent_email;
    }

    public function setAdherentPhoneNumber ($adherent_phone) {
        if (!verify_phone_number($adherent_phone)) {
            $this->_errors["incorrect_phone"] = true;
        } else {
            $this->_adherent_phone_number= $adherent_phone;
        }
        
    }
    
    public function getAdherentPhoneNumber () {
        return $this->_adherent_phone_number;
    }

    public function setAdherentAddress ($adherent_address) {
        $this->_adherent_address= $adherent_address;
    }
    
    public function getAdherentAddress () {
        return $this->_adherent_address;
    }

    public function setAdherentNumber ($adherent_number) {
        $this->_adherent_number= $adherent_number;
    }
    
    public function getAdherentNumber () {
        return $this->_adherent_number;
    }

    public function setAdherentDate ($adherent_date) {
        $this->_adherent_register_date= $adherent_date;
    }
    
    public function getAdherentDate () {
        return $this->_adherent_register_date;
    }

    public function setAdherentAdmin ($adherent_admin) {
        $this->_adherent_is_admin= $adherent_admin;
    }
    
    public function getAdherentAdmin () {
        return $this->_adherent_is_admin;
    }

    public function setAdherentSubscription ($adherent_subscription) {
        $this->_adherent_subscription= $adherent_subscription;
    }
    
    public function getAdherentSubscription () {
        return $this->_adherent_subscription;
    }

    public function setError($error) {
        $this->_errors[$error] = true;
    }
    public function getErrors()
    {
      return $this->_errors;
    }
}