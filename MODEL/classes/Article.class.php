<?php
class Article{

    private $_article_id;
    private $_article_text;
    private $_article_date;
    private $_article_author_id;
    private $_article_title;
    private $_article_adherent_only;
    private $_adherent_first_name;
    private $_adherent_last_name;


    public function __construct($valeurs = []) {
    
        if (!empty($valeurs)) // Si on a spécifié des valeurs, alors on hydrate l'objet.
        {
            $this->hydrate($valeurs);
        }
    }
    
    public function hydrate($donnees) {
        foreach ($donnees as $attribut => $valeur)
        {
            $methode = 'set'.str_replace('_', '', ucwords($attribut, '_'));
            
            if (is_callable([$this, $methode]))
            {
                $this->$methode($valeur);
            }
        }
    }


    public function setArticleId ($article_id) {
        $this->_article_id= $article_id; 
    }
    
    public function getArticleId() {
        return $this->_article_id;
    }

    public function setArticleText ($article_text) {
        $this->_article_text= $article_text; 
    }
    
    public function getArticleText() {
        return $this->_article_text;
    }
    
    public function setArticleDate ($article_date) {
        $this->_article_date= $article_date; 
    }
    
    public function getArticleDate() {
        return $this->_article_date;
    }

    public function setArticleAuthorId ($article_author_id) {
        $this->_article_author_id= $article_author_id; 
    }
    
    public function getArticleAuthorId() {
        return $this->_article_author_id;
    }

    public function setArticleTitle ($article_title) {
        $this->_article_title= $article_title; 
    }
    
    public function getArticleTitle() {
        return $this->_article_title;
    }

    public function setArticleAdherentOnly ($article_adherent_only) {
        $this->_article_adherent_only = $article_adherent_only; 
    }
    
    public function getArticleAdherentOnly() {
        return $this->_article_adherent_only;
    }

    public function setAdherentFirstName ($adherent_first_name) {
        $this->_adherent_first_name = $adherent_first_name; 
    }

    public function setAdherentLastName ($adherent_last_name) {
        $this->_adherent_last_name = $adherent_last_name; 
    }

    public function getAdherentName () {
        return $this->_adherent_first_name." ".$this->_adherent_last_name;
    }
}