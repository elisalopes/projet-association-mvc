<?php

class ArticleManager {

    private $_db;

    public function __construct($db) {
        $this->setDb($db);
    }

    public function setDb ($db) {
        $this->_db = $db;
    }

    public function createArticle($article) { // add article in db
        try{
            $query = $this->_db->prepare("
                INSERT INTO article (article_text, article_date, article_author_id, article_title, article_adherent_only) VALUES (:text, CURRENT_DATE, :author_id, :title, :adherent_only)
            ");
            $query->bindValue(':text', $article->getArticleText());
            $query->bindValue(':author_id', $article->getArticleAuthorId());
            $query->bindValue(':title', $article->getArticleTitle());
            $query->bindValue(':adherent_only', $article->getArticleAdherentOnly());
            $query->execute();
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    } 

    public function deleteArticle($id) { // delete article in db
        try{
            $query = $this->_db->exec('DELETE FROM article WHERE id = '.(int) $id);
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function countArticles($is_adherent) { // count articles in db
        $sql = 'SELECT COUNT(*) FROM article';
        if(!$is_adherent) {
            $sql .= ' WHERE article_adherent_only = 0';
        }
        return $this->_db->query($sql)->fetchColumn();
    }

    public function getArticles($start = 1, $limit = 10) { // get articles from db
        $sql = 'SELECT article.*, adherent.adherent_last_name, adherent.adherent_first_name FROM article INNER JOIN adherent ON article.article_author_id = adherent.adherent_id ORDER BY article_id DESC';

        if ($start != -1 || $limit != -1) // -1 = unlimited
        {
        $sql .= ' LIMIT '.(int) $limit.' OFFSET '.(int) $start;
        }
        
        $query = $this->_db->query($sql);
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        
        foreach ($results as $key => $value) {
            $results[$key] = new Article($value);
        }
        
        $query->closeCursor();
        
        return $results;
    }

    public function getUniqueArticle($id) { // get unique article from db
        try{
            $query = $this->_db->prepare('SELECT article.*, adherent.adherent_last_name, adherent.adherent_first_name FROM article INNER JOIN adherent ON article.article_author_id = adherent.adherent_id WHERE article_id = :id');

            $query->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $query->execute();
            
            $result = $query->fetch(PDO::FETCH_ASSOC);

            $article = new Article($result);
            
            return $article;
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function updateArticle(Article $article) { // update article in db
        try{
            $query = $this->_db->prepare('
                UPDATE article SET 
                article_text = :text, 
                article_author_id = :author_id, 
                article_title = :title, 
                article_adherent_only = :adherent_only FROM article WHERE article_id = :id
            ');

            $query->bindValue(':text', $article->getArticleText());
            $query->bindValue(':author_id', $article->getArticleAuthorId());
            $query->bindValue(':title', $article->getArticleTitle());
            $query->bindValue(':adherent_only', $article->getArticleAdherentOnly());
            $query->execute();
            $query->bindValue(':id', $article->getArticleId());
            $query->execute();
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }



}
