<?php


class DonationManager {

    private $_db;

    public function __construct($db) {
        $this->setDb($db);
    }

    public function setDb ($db) {
        $this->_db = $db;
    }


//////////////  CREATE DONATION ADHERENT //////////////////////////

    public function createDonationAdherent($password, $number) {
        if ($this->_adherent->getAdherent()){
            try{
                $query_adherent = "INSERT INTO donation (donation_last_name, donation_first_name, donation_email, donation_phone_number, donation_address, donation_id)
                SELECT adherent_last_name, adherent_first_name, adherent_email, adherent_phone_number, adherent_address, adherent_id
                FROM adherent
                WHERE adherent_id = ?";
                $query_adherent->bindValue(':id', $id);

                $query_donation = $this->_db->prepare("
                INSERT INTO donation (donation_value, donation_date) 
                VALUES ( :value, CURRENT_DATE)
            ");
            }
             catch(PDOException $e){
                echo "Erreur : " . $e->getMessage();
            }
        } else{ echo "adherent doesnt exist / wrong values";
        }
        
    }    


    public function createDonationNotAdherent($donation) {
        try{ 
            $query = $this->_db->prepare("
                INSERT INTO donation (donation_value, donation_date, donation_last_name, donation_first_name, donation_email, donation_phone_number, donation_address) 
                VALUES ( :value, CURRENT_DATE, :last_name, :first_name, :email, :phone_number, :address)
            ");

            $query->bindValue(':value', $donation->getDonationValue());
            $query->bindValue(':last_name', $donation->getDonationLastName());
            $query->bindValue(':first_name', $donation->getDonationFirstName());
            $query->bindValue(':email', $donation->getDonationEmail());
            $query->bindValue(':phone_number', $donation->getDonationPhoneNumber());
            $query->bindValue(':address', $donation->getDonationAddress());
            
            $query->execute() or die(print_r($query->errorInfo(), true));
        } catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }
    
    public function createDonationAnonymous($donation) {
        try{ 
            $query = $this->_db->prepare("
                INSERT INTO donation (donation_value, donation_date) 
                VALUES ( :value, CURRENT_DATE)
            ");

            $query->bindValue(':value', $donation->getDonationValue());
            
            $query->execute() or die(print_r($query->errorInfo(), true));
           
        } catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }
}