<?php
class CommentaryManager {
    private $_db;
    public function __construct($db) {
        $this->setDb($db);
    }
    public function setDb ($db) {
        $this->_db = $db;
    }
    public function createCommentary($commentary) { // add commentary in db
        try{
            $query = $this->_db->prepare("
                INSERT INTO commentary (commentary_date, commentary_text,commentary_validation, commentary_adherent_id, commentary_article_id) VALUES (CURRENT_DATE, :text, 0, :adherent_id, :article_id)
            ");
            $query->bindValue(':text', $commentary->getCommentaryText());
            $query->bindValue(':adherent_id', $commentary->getCommentaryAdherentId());
            $query->bindValue(':article_id', $commentary->getCommentaryArticleId());
            $query->execute();
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    } 
    public function deleteCommentary($id) { // delete commentary in db
        try{
            $query = $this->_db->exec('DELETE FROM commentary WHERE id = '.(int) $id);
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }
    public function getCommentary() { // get commentary from db
          
            try{
                $query = $this->_db->prepare('SELECT commentary_date, commentary_text, commentary_adherent_id,commentary_article_id, adherent.adherent_last_name, adherent.adherent_first_name, article.article_id FROM commentary INNER JOIN adherent ON commentary.commentary_adherent_id = adherent.adherent_id INNER JOIN article ON commentary.commentary_article_id = article.article_id ORDER BY commentary_id DESC');
             
            $query->execute();
            $results = $query->fetchAll(PDO::FETCH_ASSOC);
            
            foreach ($results as $key => $value) {
                $results[$key] = new Commentary($value);
            }

            $query->closeCursor();
            return $results;


            } catch(PDOException $e){
                echo "Erreur : " . $e->getMessage();
            }
        }
    
    public function getCommentaryArticleId($id) { // get commentaries article from db
        try{
            $query = $this->_db->prepare
            ('SELECT commentary_date, commentary_text, adherent.adherent_last_name, adherent.adherent_first_name, article.article_id 
            FROM commentary
            INNER JOIN adherent ON commentary.commentary_adherent_id = adherent.adherent_id
            INNER JOIN article ON commentary.commentary_article_id = article.article_id
            WHERE commentary_id = :id');
            $query->bindValue(':id', (int) $id, PDO::PARAM_INT);
            $query->execute();
            $result = $query->fetch(PDO::FETCH_ASSOC);
            $commentary = new Commentary ($result);
            return $commentary;
        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }
}
