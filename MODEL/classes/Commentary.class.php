
<?php
class Commentary{
    
    private $_commentary_date;
    private $_commentary_text;
    private $_commentary_validation;
    private $_commentary_adherent_id;
    private $_commentary_article_id;

    public function __construct($valeurs = []) {
    
        if (!empty($valeurs)) // Si on a spécifié des valeurs, alors on hydrate l'objet.
        {
            $this->hydrate($valeurs);
        }
    }
    
    public function hydrate($donnees) {
        foreach ($donnees as $attribut => $valeur)
        {
            $methode = 'set'.str_replace('_', '', ucwords($attribut, '_'));
            
            if (is_callable([$this, $methode]))
            {
                $this->$methode($valeur);
            }
        }
    }

    public function setCommentaryDate ($commentary_date) {
        $this->_commentary_date= $commentary_date; 
    }
    
    public function getCommentaryDate() {
        return $this->_commentary_date;
    }
    
    public function setCommentaryText ($commentary_text) {
        $this->_commentary_text= $commentary_text; 
    }
    
    public function getCommentaryText() {
        return $this->_commentary_text;
    }

    public function setCommentaryValidation($commentary_validation) {
        $this->_commentary_validation= $commentary_validation; 
    }
    
    public function getCommentaryValidation() {
        return $this->_commentary_validation;
    }

    public function setCommentaryAdherentId($commentary_adherent_id) {
        $this->_commentary_adherent_id= intval($commentary_adherent_id); 
    }
    
    public function getCommentaryAdherentId() {
        return $this->_commentary_adherent_id;
    }

    public function setCommentaryArticleId($commentary_article_id) {
        $this->_commentary_article_id= intval($commentary_article_id); 
    }
    
    public function getCommentaryArticleId() {
        return $this->_commentary_article_id;
    }

}