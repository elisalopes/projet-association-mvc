<?php

require 'MODEL/classes/Adherent.class.php'; 
require 'MODEL/classes/AdherentManager.class.php'; 
require 'CORE/connection.php';

class Profile extends Controller{

    public function main() {
        if (empty($_SESSION['adherent'])) {
            header('Location:login');
        }
        if (!empty($_POST['update_profile'])) {
            $this->updateAdherentProfile();
        } else if (!empty($_POST['update_password'])) {
            $this->updateAdherentPassword();
        }
        $this->set(array('adherent'=>$_SESSION['adherent']));
        $this->render('profile');

    }

    
    public function updateAdherentProfile() {
        $adherent = new Adherent(array(
            "adherent_id" => $_SESSION['adherent']['adherent_id'],
            "adherent_last_name" => $_POST["last_name"], 
            "adherent_first_name" => $_POST["first_name"], 
            "adherent_pseudo" => $_POST["pseudo"], 
            "adherent_password" => $_SESSION['adherent']['adherent_password'], 
            "adherent_email" => $_POST["email"], 
            "adherent_phone_number" => $_POST["phone_number"], 
            "adherent_address" => $_POST["address"], 
            "adherent_number" => $_SESSION['adherent']['adherent_number'],
            "adherent_subscription" => $_SESSION['adherent']['adherent_subscription']
        ));

        $adherent_manager = new AdherentManager(connection());

        if ($_SESSION['adherent']['adherent_pseudo'] != $adherent->getAdherentPseudo() &&$adherent_manager->existsAdherentPseudo($adherent->getAdherentPseudo())) {
            $adherent->setError("already_exists_pseudo");
        }

        if ($_SESSION['adherent']['adherent_email'] != $adherent->getAdherentEmail() &&$adherent_manager->existsAdherentEmail($adherent->getAdherentEmail())) {
            $adherent->setError("already_exists_email");
        }

        if($adherent->isValid()) {
           
            $result = $adherent_manager->updateAdherent($adherent);
            $_SESSION['adherent'] = $result; 
            $this->set(array("success_profile" => true));
           
        }
        else {
            $this->set(array("errors" => $adherent->getErrors())); // send the error to View 
        }
    }

    public function updateAdherentPassword() {
        $adherent = new Adherent(array( 
            "adherent_password" => $_POST['password']
        ));
        $errors = $adherent->getErrors();
        if ($errors['missing_password'] == false && $errors['incorrect_password'] == false) {
            $adherent_manager = new AdherentManager(connection());
            $adherent_manager->updateAdherentPassword($_SESSION['adherent']['adherent_id'], $adherent->getAdherentPassword());
            $this->set(array("success_password" => true));
        } else {
            $this->set(array("errors" => $errors));
        }

    }
    
}