<?php

require 'MODEL/classes/AdherentManager.class.php'; 
require 'CORE/connection.php';

class Login extends Controller{

    public function main() {
        if (!empty($_POST['connect'])) {
            $adherent_manager = new AdherentManager(connection());
            $adherent = $adherent_manager->getAdherent($_POST['pseudo'], $_POST['password']);
            if (empty($adherent)) {
                $this->set(array("error"=>"Mot de passe incorrect"));
            } else {
                // Enregistrer adherent dans session
                $_SESSION['adherent'] = $adherent; 
                header('Location:index');
            }
        }
        $this->render('login');
    }
}