<?php


require 'MODEL/classes/DonationPage.class.php';
require 'MODEL/classes/Adherent.class.php'; 
require 'MODEL/classes/DonationManager.class.php'; 
require 'MODEL/classes/AdherentManager.class.php';
require 'CORE/connection.php';



class Donation  extends Controller{

    public function main() {
        if (isset($_POST['donation'])) {
            $donation_manager = new DonationManager(connection());
            if ($_POST['donation_type'] == 'adherent'){
                $donation = $donation_manager->createDonationAdherent($password, $number);
                $this->set(array("success_donation" => true));
            }
            else if ($_POST['donation_type'] == 'not_adherent'){
                $donation = $donation_manager->createDonationNotAdherent($donation);
                $this->set(array("success_donation" => true));
            } 
            else if ($_POST['donation_type'] == 'anonymous'){
                $donation = $donation_manager->createDonationAnonymous($donation);
                $this->set(array("success_donation" => true));
            }
        }
        $this->render('donation');
    }
}
