<?php

class Logout extends Controller{

    public function main() {
        $_SESSION['adherent'] = NULL;
        session_destroy();
        header('Location:index');
    }
}