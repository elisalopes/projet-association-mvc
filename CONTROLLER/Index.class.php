<?php

require 'MODEL/classes/Article.class.php';
require 'MODEL/classes/ArticleManager.class.php'; 
require 'MODEL/classes/Commentary.class.php';
require 'MODEL/classes/CommentaryManager.class.php';
require 'CORE/connection.php';

class Index extends Controller{

    public function main() {

        $article_manager = new ArticleManager(connection());
        $articles = $article_manager->getArticles(0, 10);
        for ($i = 0; $i < count($articles); $i++) {
            $commentary_manager = new CommentaryManager(connection());
            $commentaries = $commentary_manager->getCommentary();
            $this->set(array('commentaries' => $commentaries));
        }
        $this->set(array('articles' => $articles));

        
        $this->render('index');
    }

    
    
}