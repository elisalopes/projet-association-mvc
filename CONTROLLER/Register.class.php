<?php

require 'MODEL/classes/Adherent.class.php'; 
require 'MODEL/classes/AdherentManager.class.php'; 
require 'CORE/connection.php';

class Register extends Controller{

    public function main() {
        if (isset($_POST['submit'])) {
            $adherent = new Adherent(array(
                "adherent_last_name" => $_POST["last_name"], 
                "adherent_first_name" => $_POST["first_name"], 
                "adherent_pseudo" => $_POST["pseudo"], 
                "adherent_password" => $_POST["password"], 
                "adherent_email" => $_POST["email"], 
                "adherent_phone_number" => $_POST["phone_number"], 
                "adherent_address" => $_POST["address"], 
                "adherent_subscription" => $_POST["subscription"]
            ));

            $adherent_manager = new AdherentManager(connection());

            if ($adherent_manager->existsAdherentPseudo($adherent->getAdherentPseudo())) {
                $adherent->setError("already_exists_pseudo");
            }
    
            if ($adherent_manager->existsAdherentEmail($adherent->getAdherentEmail())) {
                $adherent->setError("already_exists_email");
            }

            if($adherent->isValid()) {
                
                
                $result = $adherent_manager->createAdherent($adherent);
                $_SESSION['adherent'] = $result; 
                //header('Location:profile');
               
            }
            else {
                $this->set(array("errors" => $adherent->getErrors())); // send the error to View 
            }
        }
        $this->render('register');

    }
        // TODO: vérifier que le pseudo est pas déjà créé
        // if (!empty($_POST["pseudo"])) {
        //     // prévoir requête pour vérifier pseudo 
        //     $this->_errors["already_exists_pseudo"] = true;
        //     $error = true;
        // }

 
}