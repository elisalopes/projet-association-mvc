<?php

function verify_email ($input) {
    $pattern = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]{2,4}/";
    return preg_match($pattern, $input);
  
}

class Contact extends Controller{

    public function main() {

        if (isset($_POST['submit'])) {
          if ($this->checkData() == true){
            $this->sendEmail();
          } else {
              $this->set(array('error' => true));
          }
        } 
        $this->render('contact');
        
    }

    public function checkData() {
        if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message']) && verify_email($_POST['email'])) {
            return true;
        } else {
            return false;
        }
    }

    public function sendEmail() {
        $name = htmlentities($_POST['name']);
        $email = htmlentities($_POST['email']);
        $message = htmlentities($_POST['message']);
        $subject = htmlentities($_POST['subject']);
        mail('elisa.lopespanda@gmail.com', 'Contact - '.$subject.' - '.$name
        , $message);
        header('Location:contact/done');
        
    }

    public function done() {
        $this->set(array('done'=> true));
        $this->render('contact');
    }
}