<?php

include 'header.php';

?>
<!-- <style>
     article {
     background-color: white;
     background-image:("./IMAGES/membres.jpg")
     font: 1rem 'Fira Sans', sans-serif;
     /* height: max-content; */
     display: block; 
     /* border: 1px solid black; */
     min-height: 200px;
     position: relative;
     margin-top: 10px;
     padding: -10px;
     width: auto;
     }

     article img {
     float: right;
     margin-right: 10px;
     height: 100%;
     position: relative;
     top: 0px;
     }
</style> -->

     <div class="w3-light-grey w3-padding-64 w3-margin-bottom w3-center">
          <h1 class="w3-jumbo">ASSOCIATION SENEGALAISE</h1>
     </div>

          <div class="w3-row-padding w3-content" style="max-width:1400px">
               <div class="w3-twothird">
                    <img src="./IMAGES/membres.jpg" alt="equipe asso" style="width:100%">
                    <h1>L’ASSOCIATION EN DIX (10) POINTS</h1>
                         <div class="w3-justify">
                              <p>
                              Il est fondé entre les sénégalais résidant en Europe, une association, régie par la loi du 1er
                              juillet 1901 et ses décrets applicatifs, dénommée : Association Sénégalaise Pour La
                              Solidarité Et L’Entraide ; le sigle est ​ A.S.S.E​ . L’esprit d’entraide et la solidarité sont les
                              principes fondamentaux de l’A.S.S.E
                              Pour être membre de l’association, il faut s’acquitter de sa cotisation et signer le règlement
                              intérieur.</p>
                              <p> 1. Tout sénégalais de confession musulmane peut être membre, quel que soit son sexe ou
                              son âge.</p>
                              <p>  2. Le montant de la cotisation annuelle est fixée 60 euros pour une personne seule, 100
                              euros pour un couple et 5 euros par enfant de moins de 18 ans. La cotisation maximale
                              est fixée à 125 euros pour une famille avec enfants.</p>
                              <p> 3. La cotisation est non remboursable.</p>
                              <p> 4. Les membres peuvent s’acquitter de leur cotisation en une seule fois ou en trois fois
                              dans un délai maximal de trois mois.</p>
                              <p>  5. La totalité des cotisations est versée au moment de l’adhésion. Si le futur membre opte
                              pour un paiement en plusieurs fois, il devra au moins s’acquitter du premier
                              versement.</p>
                              <p>   6. Aucune restitution de cotisation n'est due au membre démissionnaire. En cas de décès,
                              la qualité de membre s'éteint avec la personne.</p>
                              <p> 7. En cas de décès d’un de ses membres à jour dans ses cotisations, l’A.S.S.E prendra en
                              charge le rapatriement du corps et le billet d’avion classe économique de la personne
                              accompagnatrice vers le pays où il sera inhumé à condition qu’il y ait suffisamment
                              d’argent dans le compte. Si l’A.S.S.E ne dispose pas d’assez de fonds pour financer le
                              rapatriement du corps, elle participera aux frais à hauteur des fonds disponibles dans le
                              compte. De plus, l’A.S.S.E convoquera une assemblée générale extraordinaire pour
                              organiser une récolte de fonds.</p>
                              <p>  8. Une fois que le membre reçoit sa carte de membre électronique il n’est plus possible
                              de réclamer la restitution du montant déjà versé.</p>
                              <p> 9. Dans le cadre de l'entraide une cotisation exceptionnelle non obligatoire peut être
                              demandée en cas de décès d’un membre, d’un de ses proches (enfant, papa ou
                              maman).</p>
                              <p> 10. Les cotisations versées dans le cadre du service rapatriement sont exclusivement
                              réservées aux rapatriements de ses membres. Elles ne peuvent pas être utilisées à
                              d’autres fins quoiqu’il en soit.</p>
          
                         </div>
               </div>
          
               <div class="w3-third">
                    <div class="w3-container w3-light-grey">
                         <h2>ASSEMBLEE GENERALE!</h2>
                         <p class="w3-justify">Nous avous le plaisir de vous convier à l'assemblée générale qui sera tenue le 30 Décembre 2020.
                         L'objet et les modalités seront précisés sur la carte d'invitation. Nous comptons sur votre mobilisation pour la réussite de l'événement</p>
                    </div>
                    <br>
                    <div class="w3-container w3-light-grey w3-justify">
                         <h2>News:Nouvelle organisation!</h2>
                         <p class="w3-justify">Les instances de décision de l’A.S.S.E sont hiérarchisées comme suit:
                              1. L’assemblée Générale
                              2. Le bureau exécutif
                              L’Assemblée Générale ordinaire, instance suprême de l’A.S.S.E, se réunit une fois tous les
                              deux ans selon les conditions décrites dans l’article 11 des statuts. Elle élit le Bureau Exécutif
                              et a la possibilité de délibérer sur n’importe quel sujet. Les décisions sont prises à la majorité
                              absolue et en cas d’égalité, la voix du président de l’Assemblée Générale est prépondérante.
                              L’Assemblée Générale extraordinaire dispose des mêmes conditions et modalités
                              d’organisation que l’Assemblée Générale ordinaire. Elle peut néanmoins être convoquée à
                              tout moment soit sur demande du bureau exécutif, soit sur demande de la moitié des membres
                              actifs à jour dans les cotisations.
                         </p>
                    </div>
                    <br>
                    <div class="w3-container w3-light-grey w3-justify">
                         <h2>New: les membres!</h2>
                         <p class="w3-justify">Tout sénégalais peut être membre, quelque soit son sexe ou son âge.
                         ● Sont membres actifs ceux qui remplissent la fiche d'adhésion et sont à jour de leurs
                         cotisations.
                         ● Les membres d'honneur : c'est un titre honorifique pour les fondateurs et les personnes
                         physiques ayant rendu d'importants services à l'association et qui la soutiennent
                         activement. Le titre de membre d'honneur est décerné par l'assemblée générale.
                         ● Les membres d’honneur ne paient pas de cotisation.
                         ● Les membres actifs doivent s'acquitter d'une cotisation annuelle.</p>
                    </div>
               </div>
          </div>

    <!-- <article> 
          <img src="./IMAGES/membres.jpg"  alt="equipe asso"></img>
          <text> 
               
                    <h1>L’ASSOCIATION EN DIX (10) POINTS</h1><br>
                    <p>
                    Il est fondé entre les sénégalais résidant en Europe, une association, régie par la loi du 1er
                    juillet 1901 et ses décrets applicatifs, dénommée : Association Sénégalaise Pour La
                    Solidarité Et L’Entraide ; le sigle est ​ A.S.S.E​ . L’esprit d’entraide et la solidarité sont les
                    principes fondamentaux de l’A.S.S.E
                    Pour être membre de l’association, il faut s’acquitter de sa cotisation et signer le règlement
                    intérieur.<br>
                    1. Tout sénégalais de confession musulmane peut être membre, quel que soit son sexe ou
                    son âge.<br>
                    2. Le montant de la cotisation annuelle est fixée 60 euros pour une personne seule, 100
                    euros pour un couple et 5 euros par enfant de moins de 18 ans. La cotisation maximale
                    est fixée à 125 euros pour une famille avec enfants.
                    3. La cotisation est non remboursable.<br>
                    4. Les membres peuvent s’acquitter de leur cotisation en une seule fois ou en trois fois
                    dans un délai maximal de trois mois.<br>
                    5. La totalité des cotisations est versée au moment de l’adhésion. Si le futur membre opte
                    pour un paiement en plusieurs fois, il devra au moins s’acquitter du premier
                    versement.<br>
                    6. Aucune restitution de cotisation n'est due au membre démissionnaire. En cas de décès,
                    la qualité de membre s'éteint avec la personne.<br>
                    7. En cas de décès d’un de ses membres à jour dans ses cotisations, l’A.S.S.E prendra en
                    charge le rapatriement du corps et le billet d’avion classe économique de la personne
                    accompagnatrice vers le pays où il sera inhumé à condition qu’il y ait suffisamment
                    d’argent dans le compte. Si l’A.S.S.E ne dispose pas d’assez de fonds pour financer le
                    rapatriement du corps, elle participera aux frais à hauteur des fonds disponibles dans le
                    compte. De plus, l’A.S.S.E convoquera une assemblée générale extraordinaire pour
                    organiser une récolte de fonds.<br>
                    8. Une fois que le membre reçoit sa carte de membre électronique il n’est plus possible
                    de réclamer la restitution du montant déjà versé.<br>
                    9. Dans le cadre de l'entraide une cotisation exceptionnelle non obligatoire peut être
                    demandée en cas de décès d’un membre, d’un de ses proches (enfant, papa ou
                    maman).<br>
                    10. Les cotisations versées dans le cadre du service rapatriement sont exclusivement
                    réservées aux rapatriements de ses membres. Elles ne peuvent pas être utilisées à
                    d’autres fins quoiqu’il en soit.</p>
          </text>

     </article>  -->

<?php

include 'footer.php';

?>