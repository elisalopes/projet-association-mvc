<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    </head> 
    <body> 
    <div class="w3-card-4" style="width:100%">
        <header class="w3-container w3-padding-32 w3-light-blue"> 
            <!-- <div class="w3-bar w3-card w3-row w3-light-blue"> -->
            <a href="index" class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green"><img src="./IMAGES/logo_ASSE.jpg" height="100px" alt="ASSE"></img></a>
            <h1>Association Sénégalaise de Solidarité et Entre-aide</h1>
            <a href="discover" class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green" >Découvrir</a>
            <a href="donation"  class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green">Dons</a>
            <a href="contact"  class="w3-bar-item  w3-card  w3-col m2 w3-center w3-hover-green">Contact</a>
                <?php
                if (empty($_SESSION['adherent'])) {
                ?>
            <a href="register" class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green" >Nouveau Adhérent</a>
            <a href="login" class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green">Se connecter</a>
                <?php 
                    // if the user is connected
                } else {
                    if ($_SESSION['adherent']['adherent_is_admin'] == 1) {
                ?> 
                    <li><a href="admin"  class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green">Admin</a></li>
                    <?php  
                        }
                    ?>
                <a href="profile"  class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green">Mon espace</a>
                <a href="logout"  class="w3-bar-item w3-card w3-col m2 w3-center w3-hover-green">Déconnexion</a>
                <?php
                }
                ?>  
            <!-- </div> -->
        </header>
        </div>
    </body>
</html>

