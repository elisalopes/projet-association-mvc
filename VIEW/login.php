<?php

    include 'header.php';
?>

<div class="w3-container">
    <h2>Welcome to your login </h2>

       <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-light-blue w3-large">Login</button>

    <div id="id01" class="w3-modal">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
    
            <div class="w3-center"><br>
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-transparent w3-display-topright" title="Close Modal">×</span>
                    <img src="./IMAGES/ibra.jpg" alt="ibrahima" style="width:20%" class="w3-circle w3-margin-top">
            </div>

            <form class="w3-container" action="" method="post">
                <?php
                    if (!empty($error)) {
                        echo "<p class='error'>$error</p>";
                        }
                ?>
                <!-- <div class="grid-container"> -->
                <div class="w3-section">
                    <!-- <div class="grid-item"> -->
                            <label for="name"><b>Pseudo:</b></label>
                        <input class="w3-input w3-border" type="text" placeholder="Enter Pseudo"name="pseudo" id="name" >
                    <!-- </div> -->

                    <!-- <div class="grid-item"> -->
                            <label for="name"><b>Mot de passe:</b></label>
                        <input class="w3-input w3-border" placeholder="Enter Password" type="password" name="password" id="password" ><img src="./IMAGES/eye-solid.svg" class="eye" alt="visible" id="toggle1"></img><br>
                    <!-- </div> -->

                    <!-- <div class="grid-item"> -->
                        <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit"value="Login">Login</button>
                        <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Remember me
                        <!-- <input type="submit" value="Login"> -->
                    <!-- </div> -->

                        <input type="hidden" name="connect" value="1">
                </div>
            </form>
            
            <script>
                    $(document).ready(function() {
                        const passwordEle = document.getElementById('password');
                        const toggleEle1 = document.getElementById('toggle1');
                        toggleEle1.addEventListener('click', function() {
                            const type = passwordEle.getAttribute('type');
                            passwordEle.setAttribute(
                                'type', type === 'password' ? 'text' : 'password'
                            );
                        });
                    });
            </script>
            
            <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>
                <span class="w3-right w3-padding w3-hide-small">Forgot <a href="#">password?</a></span>
            </div>

        </div>
    </div>
</div>

<?php

include 'footer.php';
