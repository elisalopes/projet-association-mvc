<?php
include 'header.php';
?>
    <div class="w3-container">
        <div class="w3-row">
            <?php
            for ($i = 0; $i < count($articles); $i++) {
                    
                echo "<div class='w3-col l8 s12'>";
                    echo "<div class='w3-card-4 w3-margin w3-white'>";
                            echo "<img src='IMAGES/ARTICLES/".$articles[$i]->getArticleId().".jpg' style='width:50%'>";
                        echo "<div class='w3-container'>";
                            echo "<h2>".$articles[$i]->getArticleTitle()."</h2>";
                            echo "<p>".substr($articles[$i]->getArticleText(), 0, 350)."... <a href='' class='read_more'>voir plus</a> </p>";
                            echo "<p>".$articles[$i]->getArticleText()."</p>";
                            echo "<span>".$articles[$i]->getAdherentName()." - ".$articles[$i]->getArticleDate()."</span>";
                        echo "</div>";
                    echo"</div>";
                echo "</div>";
                }
            ?>  
        </div>
    </div>
    
    <script>
        $('.read_more').click(function() {
            $(this).parent().next().show();
            $(this).parent().hide();
            return false;
        });
    </script>
    <div class="w3-container w3-padding-32 w3-margin-top">
        <button class="w3-button w3-black w3-disabled w3-padding-large w3-margin-bottom">Previous</button>
        <button class="w3-button w3-black w3-padding-large w3-margin-bottom">Next »</button>
    </div>

<?php
include 'footer.php';
?>