
<?php
include 'header.php';
?>
<form action="#" method="post"> 
<fieldset>
    <legend>Profil</legend>
<div class="informations">Vous avez choisit la formule n°<?php echo $adherent['adherent_subscription']; ?></div>
<div class="informations">Nous vous avons attribué le numéro d'adhérent : <?php echo $adherent['adherent_number']; ?></div>
    <?php
    if (!empty($success_profile)) {
      echo "<p class='success'>Votre profil a bien été mis à jour!</p>";
    }
    ?>
    <label for="last_name">Nom de famille: </label>
    <input type="text" placeholder="Votre nom" name="last_name" value="<?php echo $adherent["adherent_last_name"]; ?>" required><br>
      <?php
      if (!empty($errors) && $errors["missing_last_name"]) {
          echo "<p class='error'>Champ obligatoire</p>";
      }
      ?>

    <label for="first_name">Prénom : </label>
    <input type="text" placeholder="Votre prénom" name="first_name" value="<?php echo $adherent["adherent_first_name"]; ?>" required><br>
      <?php
      if (!empty($errors) && $errors["missing_first_name"]) {
          echo "<p class='error'>Champ obligatoire</p>";
      }
      ?>

    <label for="pseudo">Pseudo : </label>
    <input type="text" placeholder="Votre pseudo" name="pseudo" value="<?php echo $adherent["adherent_pseudo"]; ?>" required><br>
      <?php
      if (!empty($errors) && $errors["already_exists_pseudo"]) {
        echo "<p class='error'>Désolé, ce pseudo est déjà utilisé.</p>";
      } else if (!empty($errors) && $errors["missing_pseudo"]) {
        echo "<p class='error'>Champ obligatoire</p>";
    }
      ?>

    <label for="email">Adresse mail : </label>
    <input type="email" placeholder="Adresse mail" name="email" value="<?php echo $adherent["adherent_email"]; ?>"><br>
      <?php
      if (!empty($errors) && $errors["already_exists_email"]) {
        echo "<p class='error'>Désolé, cet email est déjà utilisé.</p>";
      } else if (!empty($errors) && $errors["incorrect_email"]) {
        echo "<p class='error'>Le format de l'e-mail saisi est invalide</p>";
      } else if (!empty($errors) && $errors["missing_email"]) {
        echo "<p class='error'>Champ obligatoire</p>";
      }
      ?>
    <label for="number">Téléphone : </label>
    <input type="tel" placeholder="0123456789" name="phone_number" value="<?php echo $adherent["adherent_phone_number"]; ?>" required><br>
      <?php
      if (!empty($errors) && $errors["incorrect_phone"]) {
        echo "<p class='error'>Le format du téléphone saisi est invalide</p>";
      } else if (!empty($errors) && $errors["missing_phone"]) {
        echo "<p class='error'>Champ obligatoire</p>";
      }
      ?>
    
    <label for="address">Adresse : </label>
    <input type="text" placeholder="Votre adresse" name="address" value="<?php echo $adherent["adherent_address"]; ?>" required><br>
      <?php
      if (!empty($errors) && $errors["missing_address"]) {
          echo "<p class='error'>Champ obligatoire</p>";
      }
      ?>

<p><input type="submit" name="register" value="Modifier"></p>
<input type="hidden" name="update_profile" value="1">
</fieldset>
</form>


<form action="#" method="post" id='password_form'> 
<fieldset>
    <legend>Changer mot de passe</legend>
    <?php
    if (!empty($success_password)) {
      echo "<p class='success'>Votre mot de passe a bien été modifié!</p>";
    }
    ?>
    <label for="password">Mot de passe : </label>
    <input type="password" placeholder="Mot de passe" name="password" id="password"><br>
      <?php
      if (!empty($errors) && $errors["incorrect_password"]) {
        echo "<p class='error'>Le mot de passe doit être composé de huit caractères dont une majuscule, une minuscule et au moins un chiffre</p>";
      } else if (!empty($errors) && $errors["missing_password"]) {
        echo "<p class='error'>Champ obligatoire</p>";
      }
      ?>
    <label for="confirmation_password">Confirmation mot de passe : </label>
    <input type="password" placeholder="Confirmation mot de passe" name="confirmation_password" id="confirmation_password" required><br>
    <p class='error' style='display:none' id="password_do_not_match">Les deux mots de passe ne correspondent pas!</p>
      <p><input type="submit" name="register" value="Changer"></p>
      <input type="hidden" name="update_password" value="1">
</fieldset>
</form>

<script>
  document.getElementById('password_form').onsubmit= function () {
    const password = document.getElementById('password');
    const confirmation_password = document.getElementById('confirmation_password')
    if (password.value == confirmation_password.value) {
      return true;
    } else {
      document.getElementById('password_do_not_match').style.display='block';
      return false;
    }
  }

</script>

<?php
include "footer.php";

