
<?php
include 'header.php';

    if (isset($_POST['register']) && !empty($_POST['subscription'])){
        function calculate_price($childs, $base){
        $price = $base + ($childs *5);
    if ($price  >= 125) {
        $price=125;
        }
        return $price;
    }

    if ($_POST['subscription'] == "1"){
          $base=60;
          echo "<h3>Vous devez payez la somme de ".$base." €</h3>";

        } else if ($_POST['subscription'] == "2"){
            $base=110;
            echo "<h3>Vous devez payez la somme de ".$base." €</h3>";

        } else if ($_POST['subscription'] == "3"){
            $child_number = $_POST['child_number_one'];
            $price=calculate_price($child_number, 110);
            echo "<h3>Vous devez payez la somme de ".$price." €</h3>";

        } else if ($_POST['subscription'] == "4"){
            $child_number=$_POST['child_number_two'];
            $price += calculate_price($child_number, 60);
            echo "<h3>Vous devez payez la somme de ".$price." €</h3>";

        } else {
            echo "<h3>Faites un choix parmi les formules proposées s'il vous plait.</h3>";
        }

        } else {
    ?>
<div class="w3-card-4 ">

    <div class="w3-container w3-panel w3-light-blue ">
      <h2 class="w3-center">Inscription</h2>
    </div>
      <form class="w3-container" action="#" method="post" id='register_form'> 
          <!-- <fieldset> -->
                <!-- <legend>/legend> -->
        <div class="w3-row">      
            <div class="w3-row w3-section">
              <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i>
              </div>
                <div class="w3-third">
                      <!-- <label class="w3-text-light-blue" for="last_name"><b>Nom de famille</b></label> -->
                      <input class="w3-input w3-border" type="text" placeholder="Votre nom de famille" name="last_name" value="<?php if(!empty($_POST["last_name"])){ echo $_POST["last_name"];} ?>" required><br>
                      <?php
                      if (!empty($errors) && $errors["missing_last_name"]) {
                          echo "<p class='error'>Champ obligatoire</p>";
                          }
                      ?>
                </div>
            </div>
        </div>

        <div class="w3-row">      
            <div class="w3-row w3-section">
              <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i>
              </div>
                <div class="w3-third">
                        <!-- <label class="w3-text-light-blue" 
                        for="first_name"><b>Prénom</b></label> -->
                      <input class="w3-input w3-border" type="text" placeholder="Votre prénom"      name="first_name" value="<?php if(!empty($_POST["first_name"])){ echo $_POST["first_name"];} ?>" required><br>
                          <?php
                          if (!empty($errors) && $errors["missing_first_name"]) {
                              echo "<p class='error'>Champ obligatoire</p>";
                          }
                          ?>
                </div>
            </div>
        </div>

        <div class="w3-row">      
            <div class="w3-row w3-section">
              <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i>
              </div>
                <div class="w3-third">
                        <!-- <label class="w3-text-light-blue" for="pseudo">Pseudo : </label> -->
                      <input type="text" placeholder="Votre pseudo" class="w3-input w3-border" name="pseudo" value="<?php if(!empty($_POST["pseudo"])){ echo $_POST["pseudo"];} ?>" required><br>
                            <?php
                            if (!empty($errors) && $errors["already_exists_pseudo"]) {
                              echo "<p class='error'>Désolé, ce pseudo est déjà utilisé.</p>";
                            } else if (!empty($errors) && $errors["missing_pseudo"]) {
                              echo "<p class='error'>Champ obligatoire</p>";
                          }
                            ?>
                </div>
            </div>
        </div>

        <div class="w3-row">      
              <div class="w3-row w3-section">
                <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-key"></i>
                </div>
                <div class="w3-third">
                        <!-- <label class="w3-text-light-blue"for="password"><b>Mot de passe</b></label> -->
                      <input class="w3-input w3-border" type="password" placeholder="Mot de passe" name="password" id="password" value="<?php if(!empty($_POST["password"])){ echo $_POST["password"];} ?>"required><img src="./IMAGES/eye-solid.svg" class="eye" alt="visible" id="toggle1"></img><br>
                          <?php
                            if (!empty($errors) && $errors["incorrect_password"]) {
                                echo "<p class='error'>Le mot de passe doit être composé de huit caractères dont une majuscule,
                                      une minuscule et au moins un chiffre</p>";
                            } else if (!empty($errors) && $errors["missing_password"]) {
                                echo "<p class='error'>Champ obligatoire</p>";
                            }
                          ?>
                </div>
              </div>
        </div>     

        <div class="w3-row">      
            <div class="w3-row w3-section">
              <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-key"></i>
              </div>
              <div class="w3-third">
                      <!-- <label for="confirmation_password">Confirmation mot de passe : </label> -->
                      <input class="w3-input w3-border" type="password" placeholder="Confirmation mot de passe" name="confirmation_password" id="confirmation_password" 
                      value="<?php if(!empty($_POST["confirmation_password"])){ echo $_POST["confirmation_password"];} ?>"required><img src="./IMAGES/eye-solid.svg" class="eye" alt="visible" id="toggle2"></img><br>
                        <p class='error' style='display:none' id="password_do_not_match">Les deux mots de passe ne correspondent pas!</p>
              </div>
            </div>
        </div>   

        <div class="w3-row">      
            <div class="w3-row w3-section">
                <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i>
                </div>
                <div class="w3-third">
                        <!-- <label for="email">Adresse mail: </label> -->
                      <input class="w3-input w3-border" type="email" placeholder="Adresse mail" name="email" value="<?php if(!empty($_POST["email"])){ echo $_POST["email"];} ?>" ><br>
                        <?php
                          if (!empty($errors) && $errors["already_exists_email"]) {
                              echo "<p class='error'>Désolé, cet email est déjà utilisé.</p>";
                          } else if (!empty($errors) && $errors["incorrect_email"]) {
                              echo "<p class='error'>Le format de l'e-mail saisi est invalide</p>";
                          } else if (!empty($errors) && $errors["missing_email"]) {
                              echo "<p class='error'>Champ obligatoire</p>";
                          }
                        ?>
                </div>
            </div>
        </div> 

        <div class="w3-row">
            <div class="w3-row w3-section">
              <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-phone"></i>
              </div>
              <div class="w3-third">
                    <!-- <label for="number">Téléphone : </label> -->
                    <input class="w3-input w3-border" type="tel" placeholder="0123456789" name="phone_number" value="<?php if(!empty($_POST["phone_number"])){ echo $_POST["phone_number"];} ?>" required><br>
                    <?php if (!empty($errors) && $errors["incorrect_phone"]) {
                        echo "<p class='error'>Le format du téléphone saisi est invalide</p>";
                      } else if (!empty($errors) && $errors["missing_phone"]) {
                        echo "<p class='error'>Champ obligatoire</p>";
                      }
                      ?>
              </div>
            </div>
        </div>      

        <div class="w3-row">
          <div class="w3-row w3-section">
            <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-address-card-o"></i>
            </div>
              <div class="w3-third">
                    <!-- <label for="address">Adresse : </label> -->
                      <input class="w3-input w3-border" type="text" placeholder="Votre adresse" name="address" value="<?php if(!empty($_POST["address"])){ echo $_POST["address"];} ?>" required><br>
                        <?php
                        if (!empty($errors) && $errors["missing_address"]) {
                            echo "<p class='error'>Champ obligatoire</p>";
                        }
                        ?>
              </div>
          </div>
        </div>

  <!-- <div class="w3-card-4"> -->
        <div class="w3-panel w3-padding-small w3-light-blue ">
            <h2 class="w3-container w3-center" id ="formule">Choisissez une formule:</h2>
        </div>
              <input  class="w3-radio " type="radio" id="sub_one" name="subscription" value="1">
                  <label for="one">Formule 1 : Personne seule</label><br>
              <input class="w3-radio" type="radio" id="sub_two" name="subscription" value="2">
                  <label for="two">Formule 2 : Couple sans enfant</label><br>
              <input class="w3-radio" type="radio" id="sub_three" name="subscription" value="3">
                  <label for="three">Formule 3 : Couple avec enfant de moins de 18ans</label><br>

        <div class="hide" id="three">
                  <label for="child_number">Combien d'enfants avez-vous? : </label>
              <input type="number" value="0" name="child_number_one" ><br>
        </div>
              <input class="w3-radio" type="radio" id="sub_four" name="subscription" value="4">
                  <label for="four">Formule 4 : Personne seule avec enfant de moins de 18ans</label><br>

        <div class="hide" id="four">
                  <label for="child_number">Combien d'enfants avez-vous? : </label>
              <input type="number" value="0" name="child_number_two" ><br>
        </div>
                
              <?php
                  if (!empty($errors) && $errors["missing_subscription"]) {
                    echo "<p class='error'>Champ obligatoire</p>";
                  }
              ?>

              <input type="hidden" name="submit" value="1"> 
              <!-- important! useful to know if the form was sent at least one time -->

            <script>
                document.getElementById('register_form').onsubmit= function () {
                    const password = document.getElementById('password');
                    const confirmation_password = document.getElementById('confirmation_password')
                      if (password.value == confirmation_password.value) {
                          return true;
                        } else {
                        document.getElementById('password_do_not_match').style.display='block';
                        return false;
                    }
                }

                $(document).ready(function(){ // ou $()
                    $('#accept').click(function(){
                        if ($("#register").is(':disabled')){  // : pour état
                            $('#register').removeAttr('disabled');
                        } else {
                        $('#register').attr('disabled', 'disabled');
                        }
                    });
                  
                    $('#formule').show(0); 
                    $('input[name=subscription]').click(function() {
                      if ((this.value == '1') || (this.value == '2')) {
                        $('#three').hide(500);
                        $('#four').hide(500);  
                      }
                      else if (this.value == '3') {
                            $('#three').show(500);
                            $('#four').hide(500);  
                      }
                      else if (this.value == '4') {
                          $('#four').show(500); 
                          $('#three').hide(500); 
                      }
                    });
                      const passwordEle = document.getElementById('password');
                      const confirmationEle = document.getElementById('confirmation_password');
                      const toggleEle1 = document.getElementById('toggle1');
                      const toggleEle2 = document.getElementById('toggle2');
                      
                      toggleEle1.addEventListener('click', function() {
                          const type = passwordEle.getAttribute('type');
                          passwordEle.setAttribute(
                              'type', type === 'password' ? 'text' : 'password'
                          );
                      });

                      toggleEle2.addEventListener('click', function() {
                          const type = confirmationEle.getAttribute('type');
                          confirmationEle.setAttribute(
                              'type', type === 'password' ? 'text' : 'password'
                          );
                      });
                  });

            </script>

                <br><label >J'accepte la charte de l'association </label> 
                  <input class="w3-check" type="checkbox" id="accept" name="accept" value="y"><br>
                  <a href="discover" class="remove_field"> Disponible ici</a><br><br>
                <button class="w3-btn w3-blue" id="register" disabled="disabled" name="Submit" type="submit" value="Submit">Submit</button>
      <!-- </div> -->
          <!-- </fieldset> -->
    </form>
</div>
<?php 
}
include "footer.php";
