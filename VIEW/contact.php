<?php
include 'header.php';
?>
    <div class="w3-card-4">
    <div class="w3-container w3-light-grey w3-text-blue w3-margin ">
        <h2 class="w3-center"> Nous contacter</h2>
    </div>
    <form id="contact_form" action="#" method="POST" class="w3-container w3-light-grey w3-text-blue w3-margin">
        <!-- <fieldset> -->
            <!-- <legend> Nous contacter</legend> -->
        <div class="w3-row">
            <!-- <h2 class="w3-center"> Nous contacter</h2> -->
            <?php
                if (!empty($error)) {
                    echo "<p class='error'>Veuillez remplir tous les champs du formulaire!</p>";
                }
                if (!empty($done)) {
                    echo "<p class='done'>Votre email a bien été envoyé!</p>";
                }
            ?>
                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i>
                    </div>
                        <div class="w3-rest">
                            <!-- <label for="name">Votre nom:</label><br /> -->
                                <input id="name" class="w3-input w3-border" name="name" type="text" placeholder="Votre nom" value="<?php if(!empty($_POST["name"])){ echo $_POST["name"];} ?>" required><br>
                        </div>
                    </div>
                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i>
                    </div>
                        <div class="w3-rest">
                            <!-- <label for="email">Votre email:</label><br /> -->
                                <input id="email" class="w3-input w3-border" name="email" type="text" placeholder="Email" value="<?php if(!empty($_POST["email"])){ echo $_POST["email"];} ?>" required><br><br>
                        </div>
                </div>
                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-caret-down-o"></i>
                    </div>
                    <div class="w3-rest">
                        <select class="w3-select w3-border" name="option" id="subject">
                
                            <option value="" disabled selected>Quel est le sujet de votre message ?</option>
                        
                            <option value="register">Adhésion</option>
                                    
                            <option value="donation">Dons</option>
                                
                            <option value="discover">Qui sommes-nous?</option>
                                    
                            <option value="others">Autres</option>
                   
                        </select><br><br>
                        </div>
                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-pencil"></i>
                    </div>
                        <div class="w3-rest">
                            <!-- <label class="required" for="message">Votre message:</label><br /> -->

                            <input id="message" class="w3-input w3-border " name="message" placeholder="Message" required>
                                <?php if(!empty($_POST["message"])){ echo $_POST["message"];} ?>
                            <br />
                        </div>
                </div>
                <p class="w3-center">
                    <input type="hidden" name="submit" value="1">
                    <!-- <input id="submit_button" type="submit" value="Envoyer" /> -->
                    <button class="w3-button w3-section w3-blue w3-ripple" id="submit_button" type="submit" value="Envoyer"> envoyer </button>
                </p>
            </div>
            <!-- </fieldset> -->
    </form>
</div>

<?php

include "footer.php";
?>