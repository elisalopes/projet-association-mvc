<?php
include 'header.php';  

    if (isset($_POST['donation'])){
        ?>
                <script>
                    $(document).ready(function () {
                            $('#donation_condition').hide(0);
                            $('#thanks').show()
                        });
                    });
                </script>
        <?php
                echo "<h3 name='thanks' display='none'>
                Votre don de " .$_POST["donation_value"]. " €  a bien été pris en compte.<br> 
                Nous vous en remerçions sincèrement.</h3>";

        } else {
        ?>
                <div class="w3-content w3-section" style="width:100%">
                    <img class="mySlides" src="./IMAGES/donate.jpg" style="width:75%">
                    <img class="mySlides" src="./IMAGES/donation.jpg" style="width:75%">
                    <img class="mySlides" src="./IMAGES/money.jpg" style="width:75%">
                    <img class="mySlides" src="./IMAGES/offering.jpg" style="width:75%">
                </div>

                <script>

                    var myIndex = 0;
                    carousel();
                        function carousel() {
                            var i;
                            var x = document.getElementsByClassName("mySlides");
                                for (i = 0; i < x.length; i++) {
                                    x[i].style.display = "none";  
                                }
                            myIndex++;
                            if (myIndex > x.length) {myIndex = 1}    
                            x[myIndex-1].style.display = "block";  
                            setTimeout(carousel, 2000); // Change image every 2 seconds
                        }
                </script>

            <form action="#" method="post"> 

                <fieldset id="donation_condition" class="hide">
                    <legend>Conditions du don :</legend>
                        <input type="radio" id="adherent" name="donation_type" value="adherent">
                    <label>Je suis adhérent (le don sera enregistré avec les informations personnelles saisies lors de votre inscription)</label><br>
                        <input type="radio" id="not_adherent" name="donation_type" value="not_adherent">
                    <label>Je ne suis pas adhérent (vous pourrez par la suite entrer vos informations personnelles)</label><br>
                        <input type="radio" id="anonymous" name="donation_type" value="anonymous">
                    <label>Je souhaite rester anonyme</label><br>
                        <p id="adherent"></p>
                </fieldset>
        
        
                <fieldset id="adherent_form" class="hide">  
                    <legend>Vos référence :</legend>
                        <label for="references">Entrez votre pseudo :</label>
                            <input type="text" id="adherent_pseudo" name="adherent_pseudo" value=""><br>
                    <?php
                        if (!empty($errors) && $errors["missing_pseudo"]) {
                            echo "<p class='error'>Champ obligatoire</p>";
                        } else if (!empty($errors) && $errors["incorrect_pseudo"]) {
                            echo "<p class='error'>Le pseudo saisi n'est pas répertorié</p>";
                        }
                    ?>
                    <label for="references">Entrez votre mot de passe :</label>
                        <input type="password" id="password" name="password" value="">
                            <img src="eye-solid.svg" class="eye" alt="visible" id="toggle1"></img>
                    <?php
                        if (!empty($errors) && $errors["missing_password"]) {
                            echo "<p class='error'>Champ obligatoire</p>";
                        } else if (!empty($errors) && $errors["incorrect_password"]) {
                            echo "<p class='error'>Vérifiez votre mot de passe et votre numéro d'adhérent</p>";
                        }
                    ?>
            
                <script>

                    $(document).ready(function() {
                        const passwordEle = document.getElementById('password');
                        const toggleEle1 = document.getElementById('toggle1');

                        toggleEle1.addEventListener('click', function() {
                            const type = passwordEle.getAttribute('type');
                            passwordEle.setAttribute(
                            'type', type === 'password' ? 'text' : 'password'
                            );
                        });
                    });
                </script>
            </fieldset>
        
        
            <fieldset id="not_adherent_form" class="hide">
                <legend>Informations personnelles :</legend>
                    <label for="last_name">Nom de famille :</label>
                        <input type="text" placeholder="Votre nom" name="donation_last_name" ><br>
                <?php
                    if (!empty($errors) && $errors["missing_last_name"]) {
                        echo "<p class='error'>Champ obligatoire</p>";
                    }
                ?>
            
                <label for="first_name">Prénom :</label>
                    <input type="text" placeholder="Votre prénom" name="donation_first_name"  value="
                        <?php if(!empty($_POST["donation_first_name"])){ echo $_POST["donation_first_name"];} ?>" required><br>
                    <?php
                        if (!empty($errors) && $errors["missing_donation_first_name"]) {
                            echo "<p class='error'>Champ obligatoire</p>";
                        }
                    ?>
                <label for="email">Adresse mail :</label>
                    <input type="email" placeholder="Adresse mail" name="donation_email" ><br>
                <label for="number">Téléphone :</label>
                    <input type="number" placeholder="0123456789" name="donation_tel" ><br>
                <label for="address">Adresse : </label>
                    <input type="text" placeholder="Votre adresse" name="donation_address"  ><br>
            </fieldset>
        
        
        <fieldset id="donation_form" class="hide">   
            <legend>Votre don :</legend>
                <label for="donation">Montant de votre don :</label>
                    <input type="number" id="donation_value" name="donation_value" value="">
                        <p><input type="submit" name="donation" value="Donner"></p>
                    <input type="hidden" name="error" value="1">

            <script>
                $(document).ready(function () {
                    $('#donation_condition').show(0); 
                    $('input[name=donation_type]').click(function() {
                        if (this.value == 'adherent') {
                            $('#adherent_form').show(500);
                            $('#not_adherent_form').hide(500); 
                            $('#donation_form').show(500); 
                        }
                        else if (this.value == 'not_adherent') {
                            $('#not_adherent_form').show(500); 
                            $('#donation_form').show(500); 
                            $('#adherent_form').hide(500);  
                        }
                        else if (this.value == 'anonymous') {
                            $('#donation_form').show(500); 
                            $('#adherent_form').hide(500);
                            $('#not_adherent_form').hide(500);     
                        }
                    });
                });
        
            </script>
        </fieldset>
    </form>  
    <?php
    }
    ?>

<?php
include 'footer.php';
?>