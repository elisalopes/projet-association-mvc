-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  lun. 21 sep. 2020 à 18:48
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `association`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
CREATE TABLE IF NOT EXISTS `adherent` (
  `adherent_id` int(11) NOT NULL AUTO_INCREMENT,
  `adherent_last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_pseudo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_phone_number` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adherent_number` smallint(6) NOT NULL,
  `adherent_register_date` date NOT NULL,
  `adherent_is_admin` tinyint(1) NOT NULL,
  `adherent_subscription` smallint(6) NOT NULL,
  PRIMARY KEY (`adherent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_text` text NOT NULL,
  `article_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `article_author_id` int(11) NOT NULL,
  `article_title` varchar(50) NOT NULL,
  PRIMARY KEY (`article_id`),
  KEY `fk_article_adherent_id` (`article_author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commentary`
--

DROP TABLE IF EXISTS `commentary`;
CREATE TABLE IF NOT EXISTS `commentary` (
  `commentary_id` int(11) NOT NULL AUTO_INCREMENT,
  `commentary_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `commentary_text` text NOT NULL,
  `commentary_validation` tinyint(1) NOT NULL,
  `commentary_adherent_id` int(11) NOT NULL,
  `commentary_article_id` int(11) NOT NULL,
  PRIMARY KEY (`commentary_id`),
  KEY `fk_commentary_adherent_id` (`commentary_adherent_id`),
  KEY `fk_commentary_article_id` (`commentary_article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `donation`
--

DROP TABLE IF EXISTS `donation`;
CREATE TABLE IF NOT EXISTS `donation` (
  `donation_id` int(11) NOT NULL AUTO_INCREMENT,
  `donation_value` smallint(6) NOT NULL,
  `donation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `donation_adherent_id` int(11) DEFAULT NULL,
  `donation_is_visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`donation_id`),
  KEY `fk_donation_adherent_id` (`donation_adherent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_adherent_id` FOREIGN KEY (`article_author_id`) REFERENCES `adherent` (`adherent_id`);

--
-- Contraintes pour la table `commentary`
--
ALTER TABLE `commentary`
  ADD CONSTRAINT `fk_commentary_adherent_id` FOREIGN KEY (`commentary_adherent_id`) REFERENCES `adherent` (`adherent_id`),
  ADD CONSTRAINT `fk_commentary_article_id` FOREIGN KEY (`commentary_article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `donation`
--
ALTER TABLE `donation`
  ADD CONSTRAINT `fk_donation_adherent_id` FOREIGN KEY (`donation_adherent_id`) REFERENCES `adherent` (`adherent_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
