# Projet_association

Réalisation du projet lors de la période de projets internes d'une durée de deux semaines de la formation Développeur.se WEB et WEB MOBILE par BEWEB.

Le thème du projet consistait à réaliser un site web qui permettait aux différents utilisateurs d'avoir accès à un espace membre, de pouvoir effectuer des dons, d'avoir des nouvelles de l'activité de l'association au travers de différents articles, pouvoir contacter un membre de l'association en cas de problèmes...


## Installation

- Penser à bien mettre le module MOD_REWRITE d'Apache en ENABLE
[Voici un exemple de documentation](http://www.iasptk.com/enable-apache-mod_rewrite-ubuntu-14-04-lts/)

- Penser à modifier le fichier connection dans CORE/connection.php
```php
$user = '';
$pass = '';
$dbh = new PDO('mysql:host=localhost;port=;dbname=', $user, $pass, array(1002=>'SET NAMES utf8'));
```

## Todo List : 

- <span style="color:red">RAJOUTER GET SET POUR LES 3 TYPES D'ENREGISTREMENT DE DONS</span>
    * createDonationAnonymous
    * createDonationAdherent
    * createDonationNotAdherent
<br>

- <span style="color:red">METTRE EN PLACE LES COMMENTAIRES EN DESSOUS DE CHAQUE ARTICLE</span>
    * cf. MODEL/classes/Commentary.class.php
    * cf. MODEL/classes/CommentaryManager.class.php
    * cf. CONTROLLER/Index.class.php
<br>

- <span style="color:red"> VERIFIER L'ENVOI ET LA RECEPTION DES MAILS</span>
    * cf. CONTROLLER/Contact.class.php
<br>

- <span style="color:red"> GESTION DU PAIEMENT POUR LES DONS ET LORS DE L'INSCRIPTION(ABONNEMENT)</span>
    * cf. MODEL/classes/DonationPage.class.php
    * cf. MODEL/classes/DonationManager.class.php
    * cf. MODEL/classes/Adherent.class.php
    * cf. MODEL/classes/AdherentManager.class.php
    * cf. CONTROLLER/Register.class.php
    * cf. CONTROLLER/Donation.class.php


## Credits
- Elisa LOPES (BACK-END)
- Ibrahima NDOME (FRONT-END)
- Benjamin FRICARD (FRONT-END)
