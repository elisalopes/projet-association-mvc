<?php
//pour afficher les erreurs
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// on definit des constantes pour appeller les scripts qqsoit le directory
define('WEBROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));

require(ROOT.'CORE/controller.class.php');

// on recupere tous les paramètres de l'url sépares 
if (empty($_GET['p'])) {
    $param = array('index');
} else {
    $param = explode('/',$_GET['p']);
}   

// on extrait chaque parametre 
// le premier element nous donne l'objet controleur a appeller 
$controller = ucfirst($param[0]) ;
// le deuxieme l'action a effectuer 
if (count($param) == 2) {
    $action = $param[1];
} else {
    $action = "main";
}


//
$called = 'CONTROLLER/'.$controller.'.class.php';
require($called);

// on vérifie bien que la methode appellée existe dans la classe
if ( method_exists( $controller , $action )) {
    // on l'execute
    $myctrl = new $controller() ;
    $myctrl->$action();
   
    
}
else {
    echo "methode non existante, erreur 404";
}